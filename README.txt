
-- SUMMARY --

This is an add-on module for Drupal Commerce that provides a way to display
formatted prices according to locale information.
For example, with this module a Euro product price could be displayed
as €30,000.00 for visitors from Ireland, while visitors from France would see
it as 30 000,00 €.

If your product price is not displayed according to your local information,
please create a new issue providing the correct currency format as well as local
information (language and country). Before creating this issue please check that:
  - you have set the Commerce price field formatter to "Locale formatted amount"
  - the geolocation is working as expected
  - there isn't any issue already created for your currency and locale
  information

This module also provides a countries/currencies list (see commerce_currency_locale_country_currency_get_list()),
as well as a Rules action that convert country code to its currency. Useful when
you want to set the user currency according to his/her country, works well with
the Commerce Multicurrency module.


-- REQUIREMENTS --

Commerce: http://drupal.org/project/commerce


-- INSTALLATION --

Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-7
for further information.

* Go to a product "Manage Display" tab and choose the "Locale formatted amount"
  formatter any commerce price field.

* You would need to enable at least one supported geolocation module:
  IP Geolocation Views & Maps and Smart IP work out of the box by default
  (see commerce_currency_locale.api.php to add your own).

* Or if the core PHP filter module is enabled, you can use the provided textarea
  to specify a default country code through PHP. Alternatively you can use
  hook_commerce_currency_locale_country_alter() in your module
  (see commerce_currency_locale.api.php).

* Debugging information can be displayed on the product page by ticking the debug
  checkbox.


-- API --
Check commerce_currency_locale.api.php for further information and examples.
