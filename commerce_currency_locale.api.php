<?php
/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Alter the list of countries/currencies pairs.
 *
 * This hook allows you to change or add country/currency pair.
 */
function hook_commerce_currency_locale_list_alter(&$countries_currencies) {
  $countries_currencies['FR'] = 'FRF';
}

/**
 * Defines geolocation modules.
 *
 * @return array
 *   An array of enabled module names. Each module name is an associative array,
 *   with the following key-value pairs:
 *   - title: The human-readable name of the module
 *   - callback: A callback function name
 *
 *   This callback function will optionnaly receive an IP address as an argument,
 *   and returns the ISO country code where this IP is located.
 *   If no IP address is passed as an argument this function should return the
 *   current user ISO country code.
 *
 * @see commerce_currency_locale_commerce_currency_locale_geoloc_module_info()
 * @see commerce_currency_locale_ip_geoloc_country()
 */
function hook_commerce_currency_locale_geoloc_module_info() {
  $module = array();
  if (module_exists('geoip')) {
    $module['geoip'] = array(
      'title' => 'GeoIP API',
      'callback' => 'commerce_currency_locale_geoip_country',
    );
  }
  return $module;
}

/**
 * Alter the geolocation modules list.
 *
 * This hook allows you to change the callback function of existing module
 * definitions.
 *
 * @see hook_commerce_currency_locale_geoloc_module_info()
 */
function hook_commerce_currency_locale_geoloc_module_info_alter(&$modules) {
  $modules['smart_ip']['callback'] = 'commerce_currency_locale_smart_ip_country_different';
}

/**
 * Alter the country code passed to commerce_currency_locale_currency_format().
 *
 * This hook allows you to change the country code before the commerce price
 * is formatted.
 *
 * @param array $items
 *   Array of values for the commerce price field.
 *
 * @param string $country_code
 *   The country code, passed by reference, so it can be changed.
 */
function hook_commerce_currency_locale_country_alter($items, &$country_code) {
  if ($country_code == '') {
    $country_code = 'GB';
  }
}
