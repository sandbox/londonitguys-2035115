<?php

/**
 * @file
 * Commerce Multicurrency Locale Rules integration.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_currency_locale_rules_action_info() {
  $actions = array();

  // Get the currency code according to a country code.
  $actions['commerce_currency_locale_country_code_get_currency'] = array(
    'label' => t('Get a currency code according to a country code'),
    'group' => t('Commerce Multicurrency'),
    'parameter' => array(
      'country_code' => array(
        'type' => 'text',
        'label' => t('ISO country code'),
        'description' => t('The <a href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2" title="ISO 3166-1 alpha-2 country code" target="_blank">ISO 3166-1 alpha-2 country code</a>.'),
      ),
    ),
    'provides' => array(
      'currency_code' => array(
        'type' => 'text',
        'label' => t('Currency code'),
      ),
    ),
  );

  return $actions;
}

/**
 * The action function for the 'commerce_currency_locale_country_code_get_currency'.
 *
 * Return the currency code used in a country.
 *
 * @param string $country_code
 *   ISO 3166-1 alpha-2 country code.
 *
 * @return string|NULL
 *   ISO 4217 currency code or NULL if not found.
 */
function commerce_currency_locale_country_code_get_currency($country_code) {
  $countries_currencies = commerce_currency_locale_country_currency_get_list();
  return array(
    'currency_code' => isset($countries_currencies[$country_code]) ? $countries_currencies[$country_code] : NULL,
  );
}
